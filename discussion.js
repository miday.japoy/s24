db.inventory.insertMany([

	{
		"name" : "Captain America Shield",
		"price" : 50000,
		"qty" : 17,
		"company" : "Hydra and Go"
	},

	{
		"name" : "Mjolnir",
		"price" : 75000,
		"qty" : 24,
		"company" : "Asgard Production"
	},

		{
		"name" : "Iron Man Suit",
		"price" : 25400,
		"qty" : 24,
		"company" : "Stark Industries"
	},

	{
		"name" : "Eye of Agamoto",
		"price" : 10000,
		"qty" : 51,
		"company" : "Sanctum company"
	},


	{
		"name" : "Iron Spider Suit",
		"price" : 30000,
		"qty" : 21,
		"company" : "Stark Industries"
	}



	])




// Query Operators
	//allows us to be more flexible when querying in MongoDB, we can opt to find, update and delete documents bsed on soe condition instead of just specifiv criterias.

// Comparison Query operators
	// $gt and $gte
	// Syntax

	db.collections.find({field: {$gt: value}});
	db.collection.find({field: {$gte: value}});

// $gt- greater than, allows us to find vales that are greater than the given value
// $gte- greater than or equal, allows us to find values that are greater than the given value.


db.inventory.find({price: {$gte: 75000}});

//  $lte and @lte
// syntax:
	db.collections.find({field: {$lt: value}});
	db.collections.find({field: {$lte: value}});


// $lt - less than, allows us to find values that are less than the given value
// $lte - less than or equal, allows us to find values that are less than or equal the given value

db.inventory.find({"qty": {$lt: 20}});

// $ne - not equal
// syntax :
db.collections.find({field: {$ne: value}});



db.inventory.find({qty: {$ne: 10}});


//  $in  - or
//  Syntax:
db.collections.find({field: {in: {[value]}})

// $in - allows us to find documents that satisfy either of the specified values.

db.inventory.find({price: {$in: [25400, 30000]}})


db.invent({field: {$gte: value}});


db.inventory.find({qty: {$in: [20, 16]}})

// UPDATE AND DELETE
	//  2 arguments namely: query criteria, update

	db.inventory.updateMany({qty: {$gte: 50}}, {$set: {isActive: false}});



	// updating the qty to 17 with less than 28000 of price

	db.inventory.updateMany({price: {$lt: 28000}}, {$set: {qty: 17}});



// Logical Operators
	// $and
	// Syntax:
		db.collections.find({$and: [{},{}]})

	// $and0 allows us to return documents that satisfies all give conditions

	db.inventory.find({$and: [{price: {$gte: 50000}},{qty: 17}]});

// $or
// syntax
	db.collections.find({$or: [{},{}]})

//  $or - allows us to return documents that satisfies either of the given conditions

db.inventory.find({$or: [{qty: {$lt: 24}}, {isActive: false}]});



//mini-activity

db.inventory.updateMany({$or: [{qty: {$lte: 24}}, {price: {lte: 30000}}]}, {$set: {isActive: true}});



//  Evaluation Query Operator

	//  $regex
		// Syntax
		{field:{regex: /pattern/}}
			// case sensitive query

		{field: {@regex: /pattern/, $options: '$potionValue'}}
			// case insensitive query


		db.inventory.find({name: {$regex: 'S'}})

		db.inventory.find({name: {$regex: 'S', $options: '$i'}})



// 

db.inventory.find({$and: [{name: {$regex: 'i'}}, {price: {$gt: 70000}}]})




// Field Projection
	//allows us to hide/show properties of a returned documents after a query. When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish

	// Inclusions and Exclusion
	// syntax
		db.collections.find({criteria}, {field: 1})
			//field: 1 - allows us to include/add specific fields only when retrieving documents. The value provided is 1 to denote that the field is being included.

		db.inventory.find({}, {name: 1})

	// syntax
		db.collections.find({criteria}, {field: 0})

			//field: 0 - allows us to exclude specific fields.

			db.inventory.find({}, {qty: 0})

			db.inventory.find({}, {_id: 0, name: 1})

		// reminder: when using field projection, field inclusion and exclusion may not be used in the same time. Excluding "_id" field.

		db.inventory.find({company: {$regex: 'A'}}, {name: 1, company: 1, _id: 0})
